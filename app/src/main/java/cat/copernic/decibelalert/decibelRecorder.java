package cat.copernic.decibelalert;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class decibelRecorder extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
    private TextView textoUsuario, textoNombre;

    private String variable_usuario;
    private String variable_nombre;
    private String variable_apellido;
    private String variable_correo;
    private String variable_sexe;
    private String variable_password;

    public static final String intentUsuario = "variable_usuario";
    public static final String intentNombre = "variable_nom";
    public static final String intentApellido = "variable_cog";
    public static final String intentImagen = "variable_imagen";
    public static final String intentCorreo = "variable_correo";
    public static final String intentSexe = "variable_sexe";
    public static final String intentPassword = "variable_password";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decibel_recorder);


        String recuperar_variable_usuario = getIntent().getStringExtra(intentUsuario);
        String recuperar_variable_nombre = getIntent().getStringExtra(intentNombre);
        String recuperar_variable_apellido = getIntent().getStringExtra(intentApellido);
        String recuperar_variable_correo = getIntent().getStringExtra(intentCorreo);
        String recuperar_variable_sexe = getIntent().getStringExtra(intentSexe);
        String recuperar_variable_contrasena = getIntent().getStringExtra(intentPassword);

        variable_usuario = recuperar_variable_usuario;
        variable_nombre = recuperar_variable_nombre;
        variable_apellido = recuperar_variable_apellido;
        variable_correo = recuperar_variable_correo;
        variable_sexe = recuperar_variable_sexe;
        variable_password = recuperar_variable_contrasena;

        /*Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        textoUsuario = headerView.findViewById(R.id.textUsuario);
        textoNombre = headerView.findViewById(R.id.textNombre);
        textoUsuario.setText(variable_usuario);
        textoNombre.setText(variable_nombre + " "+variable_apellido);
        navigationView.setNavigationItemSelectedListener(this);

        /*ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();*/

        if(savedInstanceState == null) {
            AudioFragment fragment = new AudioFragment();

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.fragment_container,fragment,null);
            fragmentTransaction.commit();
            navigationView.setCheckedItem(R.id.nav_audio);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()){
            case R.id.nav_audio:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new AudioFragment()).commit();
                break;
            case R.id.nav_profile:
                ProfileFragment fragment = new ProfileFragment();
                Bundle bundle = new Bundle();
                bundle.putString("user", variable_usuario);
                bundle.putString("nombre", variable_nombre);
                bundle.putString("apellido",variable_apellido);
                bundle.putString("correo",variable_correo);
                bundle.putString("sexe",variable_sexe);
                bundle.putString("password",variable_password);
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(R.id.fragment_container,fragment,null);
                fragmentTransaction.commit();
                break;

            case R.id.nav_conexion:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new conexionFragment()).commit();
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public String getDataFragment(String variable){
        return variable;
    }

    /*@Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            iniciarAudio();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        detenerAudio();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void iniciarAudio(){

        if(sonido == null){
            sonido = new MediaRecorder();
            sonido.setAudioSource(MediaRecorder.AudioSource.UNPROCESSED);
            sonido.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            sonido.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            sonido.setOutputFile("/dev/null");

            try {
                sonido.prepare();
            } catch (java.io.IOException e) {
                android.util.Log.getStackTraceString(e);
            } catch (java.lang.SecurityException e){
                android.util.Log.getStackTraceString(e);
            }
            try {
                sonido.start();
            }catch (java.lang.SecurityException e){
                android.util.Log.getStackTraceString(e);
            }
        }
    }

    public void detenerAudio(){
        if(sonido !=null){
            sonido.stop();
            sonido.release();
            sonido = null;
        }
    }

    @SuppressLint("SetTextInt")
    public void actualizaAudio(){
        DecimalFormat df1 = new DecimalFormat("####.0");
        double decib = valorDb(20);
        String decib1 = df1.format(decib);
        barraSonido.setProgress((int)decib);
        valorDb.setText(decib1 + "db");

        if(decib < 10){
            referencia.setText("Casi silencio");
            valorDb.setTextColor(Color.rgb(0,132,194));
        }
        else if (decib < 20){
            referencia.setText("Sonido reloj");
            valorDb.setTextColor(Color.rgb(0,190,194));
        }
        else if (decib < 30){
            referencia.setText("Susurro");
            valorDb.setTextColor(Color.rgb(0,194,121));
        }
        else if (decib < 40){
            referencia.setText("Comienzo de Ruido");
            valorDb.setTextColor(Color.rgb(0,194,68));
        }
    }

    private double valorDb(double amperio) {
    double dbsqpl = 20 * Math.log10(presionSonido() / amperio);
    if (dbsqpl < 0){
        return 0;
    }
    else {
        return dbsqpl;
    }
    }

    public double getAmplitude(){
        if(sonido != null){
            return(sonido.getMaxAmplitude());
        }
        else {
            return 0;
        }
    }

    public double presionSonido(){
        double amperio = getAmplitude();
        ambiente = filtro * amperio + (1.0 - filtro) * ambiente;
        return ambiente;
    }*/


}
