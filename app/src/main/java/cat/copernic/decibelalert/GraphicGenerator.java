package cat.copernic.decibelalert;

import android.graphics.Color;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;


public class GraphicGenerator extends AppCompatActivity {
    LineGraphSeries<DataPoint> series;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graphicgenerator);
        int [] recuperar_datos = getIntent().getIntArrayExtra("vector_media");
        int y,x;
        x= 0;
        y = 0;
        GraphView graph = (GraphView) findViewById(R.id.graph);
        series = new LineGraphSeries<DataPoint>();
        for (int i=0; i<20;i++) {
            y = recuperar_datos[i];
            x = i;
            if(recuperar_datos[i] >50){
                series.setColor(Color.RED);
                Toast.makeText(getApplicationContext(),"Ha sobrepasat els 50 db!!",Toast.LENGTH_SHORT).show();
            }
            series.appendData(new DataPoint(x,y),true, 20);
        }
            graph.addSeries(series);
    }


    }
