package cat.copernic.decibelalert;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cat.copernic.decibelalert.BBDDsqlite.BDProjecte;

public class cambiarPerfil extends AppCompatActivity {

    private EditText nomCambiar, apellidoCambiar, usuarioCambiar, correoCambiar, sexeCambiar, passwordCambiar;
    private Button btCanviar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambiar_perfil);
        nomCambiar = findViewById(R.id.etNomCambiar);
        apellidoCambiar = findViewById(R.id.etApellidoCambiar);
        btCanviar = findViewById(R.id.btnEditarPerfil);
        usuarioCambiar = findViewById(R.id.etUsuariCambiar);
        correoCambiar = findViewById(R.id.etCorreuCambiar);
        sexeCambiar = findViewById(R.id.etSexeCambiar);
        passwordCambiar = findViewById(R.id.etPasswordCambiar);

        final BDProjecte bdprojecte = new BDProjecte(getApplicationContext());


            recuperarContrasenya();
            recuperarContrasenyaPerfil();


        btCanviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bdprojecte.editarDatos(usuarioCambiar.getText().toString(), nomCambiar.getText().toString(),apellidoCambiar.getText().toString());
                Toast.makeText(getApplicationContext(),"S'ha modificat correctament!!",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getBaseContext(),MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private void recuperarContrasenya(){
        String recuperar_variable_usuario = getIntent().getStringExtra("variable_usuario_final");
        usuarioCambiar.setText(recuperar_variable_usuario);
        String recuperar_variable_correo = getIntent().getStringExtra("variable_correo_final");
        correoCambiar.setText(recuperar_variable_correo);
        String recuperar_variable_sexe = getIntent().getStringExtra("variable_sexe_final");
        sexeCambiar.setText(recuperar_variable_sexe);
        String recuperar_variable_nom = getIntent().getStringExtra("variable_nom_final");
        nomCambiar.setText(recuperar_variable_nom);
        String recuperar_variable_cognom = getIntent().getStringExtra("variable_cognom_final");
        apellidoCambiar.setText(recuperar_variable_cognom);
        String recuperar_variable_password = getIntent().getStringExtra("variable_password_final");
        passwordCambiar.setText(recuperar_variable_password);
    }

    private void recuperarContrasenyaPerfil(){
        String usuari = getIntent().getStringExtra("variable_cambiarUsuario");
        String nombre = getIntent().getStringExtra("variable_cambiarNom");
        String apellido = getIntent().getStringExtra("variable_cambiarApellido");
        String correo = getIntent().getStringExtra("variable_cambiarCorreo");
        String sexe = getIntent().getStringExtra("variable_cambiarSexe");
        String password = getIntent().getStringExtra("variable_cambiarPassword");

        nomCambiar.setText(nombre);
        apellidoCambiar.setText(apellido);
        usuarioCambiar.setText(usuari);
        correoCambiar.setText(correo);
        sexeCambiar.setText(sexe);
        passwordCambiar.setText(password);
    }
}
