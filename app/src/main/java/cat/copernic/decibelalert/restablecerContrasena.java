package cat.copernic.decibelalert;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import cat.copernic.decibelalert.BBDDsqlite.BDProjecte;

public class restablecerContrasena extends AppCompatActivity {
private Button btCerca;
private EditText etCorreu;
private Cursor validacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restablecer_contrasena);
        btCerca = findViewById(R.id.btnCercar);
        etCorreu = findViewById(R.id.etCorreoRecuperar);

        btCerca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BDProjecte bdprojecte = new BDProjecte(getApplicationContext());
                SQLiteDatabase db=bdprojecte.getWritableDatabase();
                String restablecer = etCorreu.getText().toString();
                validacion=db.rawQuery("SELECT * FROM USUARIOS WHERE CORREO='"+restablecer+"'",null);
                if(validacion.moveToLast()) {
                    int foto = validacion.getInt(0);
                    String usua = validacion.getString(1);
                    String pass = validacion.getString(2);
                    String sex = validacion.getString(3);
                    String corr = validacion.getString(4);
                    String nom = validacion.getString(5);
                    String cog = validacion.getString(6);
                    if(restablecer.equals(corr)){

                        Intent intent = new Intent(getBaseContext(), cambiarPerfil.class);
                        intent.putExtra("variable_usuario_final",usua);
                        intent.putExtra("variable_password_final",pass);
                        intent.putExtra("variable_correo_final",corr);
                        intent.putExtra("variable_sexe_final",sex);
                        intent.putExtra("variable_nom_final",nom);
                        intent.putExtra("variable_cognom_final",cog);
                        startActivity(intent);

                        etCorreu.setText("");
                    }
                }
                else{
                    Toast.makeText(getApplicationContext(), "EL CORREO NO EXISTE, VUELVE A INTRODUCIR EL CORREO", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_restablecer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menu_crear_usuario:
                Intent intent = new Intent(this,crearUsuari.class);
                startActivity(intent);
                return true;

            case R.id.menu_iniciar_sesion:
                Intent intent1 = new Intent(this, MainActivity.class);
                startActivity(intent1);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
