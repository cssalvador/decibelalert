package cat.copernic.decibelalert.BBDDsqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class BDProjecte extends SQLiteOpenHelper {
    private static final String NOM_BD="finalBBDD";
    private static final int VERSION_BD=1;
    private static final String TAULA_BD="CREATE TABLE USUARIOS(FOTO BLOG, USUARIO TEXT PRIMARY KEY, PASSWORD TEXT, SEXE TEXT, CORREO TEXT, NOM TEXT, COGNOM TEXT)";
    private static final String SQLonUpgrade = "DROP TABLE IF EXISTS USUARIOS";
    private static final String SQLagregarCampos = "INSERT INTO USUARIOS VALUES";
    private static final String SQLmostrarUsuarios = "SELECT * FROM USUARIOS";

    public BDProjecte(Context context){
        super(context,NOM_BD,null,VERSION_BD);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TAULA_BD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQLonUpgrade+TAULA_BD);
        db.execSQL(TAULA_BD);
    }

    public void agregarCampos(int foto, String usuario,String password,String sexe,String correo, String nom, String cognom){
        SQLiteDatabase bd=getWritableDatabase();
        if(bd!=null){
            bd.execSQL(SQLagregarCampos+"('"+foto+"','"+usuario+"','"+password+"','"+sexe+"','"+correo+"','"+nom+"','"+cognom +"')");
            bd.close();
        }
    }

    public List<BDModelo> mostrarUsuarios(){
        SQLiteDatabase bd=getWritableDatabase();
        Cursor cursor = bd.rawQuery(SQLmostrarUsuarios,null);
        List<BDModelo> usuarios= new ArrayList<>();
        if(cursor.moveToFirst()){
            do {
                usuarios.add(new BDModelo(cursor.getBlob(0), cursor.getString(1),cursor.getString(2)
                ,cursor.getString(3),cursor.getString(4),cursor.getString(5),
                        cursor.getString(6)));
            }while (cursor.moveToNext());
        }
        return usuarios;
    }

    public void editarDatos(String usuario, String nom, String cognom){
        SQLiteDatabase bd=getWritableDatabase();
        if(bd!=null){
            bd.execSQL("UPDATE USUARIOS SET NOM='"+nom+"',COGNOM='"+cognom+"'WHERE USUARIO='"+usuario+"'");
            bd.close();
        }
    }
}
