package cat.copernic.decibelalert.BBDDsqlite;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cat.copernic.decibelalert.R;

public class BDAdaptador extends RecyclerView.Adapter<BDAdaptador.ViewHolder> {

   public static class ViewHolder extends RecyclerView.ViewHolder{
       private TextView tUsuari,tCorreu,tSexe,tNom,tCognom,tPassword;
       private ImageView imgFoto;
       public ViewHolder(View v){
           super(v);
           imgFoto = (ImageView)v.findViewById(R.id.imgFoto);
           tUsuari =(TextView)v.findViewById(R.id.tvUsuari);
           tCorreu = (TextView)v.findViewById(R.id.tvCorreu);
           tSexe =(TextView)v.findViewById(R.id.tvSexe);
           tNom = (TextView)v.findViewById(R.id.tvNom);
           tCognom =(TextView)v.findViewById(R.id.tvCognom);
           tPassword=(TextView)v.findViewById(R.id.tvPassword);
       }
   }

   public List<BDModelo> listabd;

   public BDAdaptador(List<BDModelo> listabd){
       this.listabd = listabd;
   }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_bd,viewGroup,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
       // holder.imgFoto.setImageBitmap(listabd.get(position).getFoto());
        holder.tUsuari.setText(listabd.get(position).getUsuari());
        holder.tPassword.setText(listabd.get(position).getPassword());
        holder.tSexe.setText(listabd.get(position).getGenero());
        holder.tCorreu.setText(listabd.get(position).getCorreo());
        holder.tNom.setText(listabd.get(position).getNomPersona());
        holder.tCognom.setText(listabd.get(position).getCognomPersona());
    }

    @Override
    public int getItemCount() {
        return listabd.size();
    }
}
