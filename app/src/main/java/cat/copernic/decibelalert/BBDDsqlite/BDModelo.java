package cat.copernic.decibelalert.BBDDsqlite;

public class BDModelo {
    private String usuario,sexe,genero,nomPersona, cognomPersona,correo,password;
    private byte[] foto;

    public BDModelo(){

    }

    public BDModelo(byte[] foto,String usuario,String password,String genero,String correo,String nomPersona, String cognomPersona){
        this.foto = foto;
        this.usuario = usuario;
        this.password = password;
        this.genero = genero;
        this.correo = correo;
        this.nomPersona = nomPersona;
        this.cognomPersona = cognomPersona;

    }

    public byte[] getFoto(){return  foto;}

    public String getPassword(){
        return password;
    }

    public String getUsuari(){
        return usuario;
    }

    public String getGenero(){
        return genero;
    }

    public String getCorreo(){return correo;}

    public String getNomPersona(){return nomPersona;}

    public String getCognomPersona(){return cognomPersona;}



}
