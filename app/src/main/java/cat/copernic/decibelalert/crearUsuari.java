package cat.copernic.decibelalert;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

import cat.copernic.decibelalert.BBDDsqlite.BDProjecte;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class crearUsuari extends AppCompatActivity {
    Button crearUser, botonCamara;
    EditText usuario, password, sexe, correo, nom, cognom, contraRepetida;
    ImageView imFoto;
    private final String CARPETA_RAIZ="misImagenesPrueba/";
    private final String RUTA_IMAGEN=CARPETA_RAIZ+"fotosDecibelAlert";

    final int COD_SELECCIONA=10;
    final int COD_FOTO=20;
    String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_usuari);
        botonCamara = (Button) findViewById(R.id.btnCamara);
        imFoto = (ImageView) findViewById(R.id.creaPerfil);
        crearUser = (Button) findViewById(R.id.btnCrearUsuari);
        usuario = (EditText) findViewById(R.id.etUsuari);
        password = (EditText) findViewById(R.id.etContrasena);
        sexe = (EditText) findViewById(R.id.etSexe);
        correo = (EditText) findViewById(R.id.etCorreo);
        nom = (EditText) findViewById(R.id.etNom);
        cognom = (EditText) findViewById(R.id.etCognom);
        contraRepetida = (EditText) findViewById(R.id.etContrasenaRepetida);

        if(validaPermisos()){
            botonCamara.setEnabled(true);

        }else {
            botonCamara.setEnabled(false);
        }
        if(imFoto == null){
            Toast.makeText(getApplicationContext(),"Has de posar una imatge!!",Toast.LENGTH_LONG).show();
        }
        botonCamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarImagen();
            }
        });

        final BDProjecte bdprojecte = new BDProjecte(getApplicationContext());

        crearUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bdprojecte.agregarCampos(imFoto.getImageAlpha(), usuario.getText().toString(), password.getText().toString(),
                        sexe.getText().toString(), correo.getText().toString(), nom.getText().toString(),
                        cognom.getText().toString());

                if (contraRepetida.getText().toString().equalsIgnoreCase(password.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "Se agrego el Usuario correctamente!!", Toast.LENGTH_LONG).show();
                    usuario.setText("");
                    password.setText("");
                    sexe.setText("");
                    correo.setText("");
                    nom.setText("");
                    cognom.setText("");
                    contraRepetida.setText("");

                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    startActivity(intent);
                } else {
                    contraRepetida.setText("");
                    Toast.makeText(getApplicationContext(), "El password no corresponde con el que has puesto", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private boolean validaPermisos() {

        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.M){
            return true;
        }

        if((checkSelfPermission(CAMERA)== PackageManager.PERMISSION_GRANTED)&&
                (checkSelfPermission(WRITE_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED)){
            return true;
        }

        if((shouldShowRequestPermissionRationale(CAMERA)) ||
                (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE))){
            cargarDialogoRecomendacion();
        }else{
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,CAMERA},100);
        }

        return false;
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode==100){
            if(grantResults.length==2 && grantResults[0]==PackageManager.PERMISSION_GRANTED
                    && grantResults[1]==PackageManager.PERMISSION_GRANTED){
                botonCamara.setEnabled(true);
            }else{
                solicitarPermisosManual();
            }
        }

    }

    private void solicitarPermisosManual() {
        final CharSequence[] opciones={"si","no"};
        final AlertDialog.Builder alertOpciones=new AlertDialog.Builder(crearUsuari.this);
        alertOpciones.setTitle("¿Desea configurar los permisos de forma manual?");
        alertOpciones.setItems(opciones, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opciones[i].equals("si")){
                    Intent intent=new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri=Uri.fromParts("package",getPackageName(),null);
                    intent.setData(uri);
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(),"Los permisos no fueron aceptados",Toast.LENGTH_SHORT).show();
                    dialogInterface.dismiss();
                }
            }
        });
        alertOpciones.show();
    }

    private void cargarDialogoRecomendacion() {
        AlertDialog.Builder diag = new AlertDialog.Builder(crearUsuari.this);
        diag.setTitle("Permisos Desactivados");
        diag.setMessage("Debe aceptar los permisos para el correcto funcionamiento de la App");

        diag.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,CAMERA},100);
                }
            }
        });
        diag.show();
    }


    private void cargarImagen() {

        final CharSequence[] opciones={getString(R.string.Take_foto),getString(R.string.add_foto),getString(R.string.exit)};
        final AlertDialog.Builder alertOpciones=new AlertDialog.Builder(crearUsuari.this);
        alertOpciones.setTitle(R.string.Escojer_opcion);
        alertOpciones.setItems(opciones, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opciones[i].equals("Fer Foto") || opciones[i].equals("Hacer Foto") || opciones[i].equals("Take Photo")){
                    tomarFotografia();
                }else{
                    if (opciones[i].equals("Carrega Imatge") || opciones[i].equals("Añadir Foto") || opciones[i].equals("Add Photo")){
                        Intent intent=new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        //intent.setType("image/");
                        startActivityForResult(intent.createChooser(intent,":)"),COD_SELECCIONA);
                    }else{
                        dialogInterface.dismiss();
                    }
                }
            }
        });
        alertOpciones.show();

    }
    private void tomarFotografia() {
        File fileImagen=new File(Environment.getExternalStorageDirectory(),RUTA_IMAGEN);
        boolean isCreada=fileImagen.exists();
        String nombreImagen="";
        if(isCreada==false){
            isCreada=fileImagen.mkdirs();
        }

        if(isCreada==true){
            nombreImagen=(System.currentTimeMillis()/1000)+".jpg";
        }


        path=Environment.getExternalStorageDirectory()+
                File.separator+RUTA_IMAGEN+File.separator+nombreImagen;
        File imagen = new File(path);


        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N)
        {
            String authorities=getApplicationContext().getPackageName()+".provider";
            Uri imageUri= FileProvider.getUriForFile(this,authorities,imagen);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        }else
        {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imagen));
        }
        startActivityForResult(intent,COD_FOTO);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode==RESULT_OK){

            switch (requestCode){
                case COD_SELECCIONA:
                    Uri miPath=data.getData();
                    imFoto.setImageURI(miPath);
                    break;

                case COD_FOTO:
                    MediaScannerConnection.scanFile(getApplicationContext(), new String[]{path}, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                @Override
                                public void onScanCompleted(String path, Uri uri) {
                                    Log.i("R","Path: "+path);
                                }
                            });

                    Bitmap bitmap= BitmapFactory.decodeFile(path);

                    //Bitmap bitmap = (Bitmap)data.getExtras().get("data");
                    imFoto.setImageBitmap(bitmap);
                    break;
            }


        }
    }

}
