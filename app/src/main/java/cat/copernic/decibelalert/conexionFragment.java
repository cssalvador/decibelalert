package cat.copernic.decibelalert;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class conexionFragment extends Fragment {
    TextView textResponse;
    EditText editTextAddress, editTextPort;
    Button buttonConnect, buttonClear;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_conexion, container, false);
        editTextAddress = (EditText) rootView.findViewById(R.id.address);
        editTextPort = (EditText) rootView.findViewById(R.id.port);
        buttonConnect = (Button) rootView.findViewById(R.id.connect);


        buttonConnect.setOnClickListener(buttonConnectOnClickListener);

        return rootView;
    }

    View.OnClickListener buttonConnectOnClickListener =
            new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    /*
                     * You have to verify editTextAddress and
                     * editTextPort are input as correct format.
                     */
                    String ipaddress = editTextAddress.getText().toString().trim();
                    int port = Integer.parseInt(editTextPort.getText().toString());

                    AudioFragment audioFragment = new AudioFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("ipAdresa",ipaddress);
                    bundle.putInt("ipPort",port);
                    audioFragment.setArguments(bundle);

                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.add(R.id.fragment_container,audioFragment,null);
                    fragmentTransaction.commit();
                   /* MyClientTask myClientTask = new MyClientTask(
                            editTextAddress.getText().toString(),
                            Integer.parseInt(editTextPort.getText().toString()));
                    myClientTask.execute();*/
                }
            };
}
