package cat.copernic.decibelalert;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class MostrarDataJson extends AppCompatActivity  {
    private String TAG = MostrarDataJson.class.getSimpleName();
    private ListView lv;

    ArrayList<HashMap<String, String>> capturalist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mostrardatajson);

        capturalist = new ArrayList<>();
        lv = (ListView) findViewById(R.id.list);

        new GetCapturas().execute();
    }

    private class GetCapturas extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(MostrarDataJson.this,"Json Data is downloading",Toast.LENGTH_LONG).show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();
            // Making a request to url and getting response
            String url = "http://192.168.12.178/capturas/fetch_captura.php";
            String jsonStr = sh.makeServiceCall(url);

            Log.e(TAG, "Response from url: " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray data = jsonObj.getJSONArray("data");
                    int j = 0;
                    int vectorMediaSonido [] = new int[20];
                    // looping through All Contacts
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject c = data.getJSONObject(i);
                        Integer idCaptura = c.getInt("idCaptura");
                        String data_hora = c.getString("data_hora");
                        Integer mediaSonido = c.getInt("mediaSonido");
                         if (i >= data.length()-20){
                                vectorMediaSonido[j] = mediaSonido;
                                j=j+1;
                         }


                        // tmp hash map for single contact
                        HashMap<String, String> contact = new HashMap<>();

                        // adding each child node to HashMap key => value
                        contact.put("idCaptura", idCaptura.toString());
                        contact.put("data_hora", data_hora);
                        contact.put("mediaSonido", mediaSonido.toString());
                        // adding contact to contact list
                        capturalist.add(contact);
                    }
                    Intent intent = new Intent(getApplicationContext(),GraphicGenerator.class);
                    intent.putExtra("vector_media",vectorMediaSonido);
                    startActivity(intent);
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });

                }

            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            ListAdapter adapter = new SimpleAdapter(MostrarDataJson.this, capturalist,
                    R.layout.list_item, new String[]{ "idCaptura","mediaSonido","data_hora"},
                    new int[]{R.id.id_Captura, R.id.media_Sonido,R.id.data_hora});
            lv.setAdapter(adapter);
        }

    }
}
