package cat.copernic.decibelalert;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Locale;

import cat.copernic.decibelalert.BBDDsqlite.BDProjecte;

public class MainActivity extends AppCompatActivity {
    private EditText eUsername, ePassword;
    private String LANG_CURRENT = "en";
    private Button btLogin, btLogarse, btMostrar;
    private Cursor validacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        eUsername = findViewById(R.id.etUsername);
        ePassword = findViewById(R.id.etPasswordPerfil);
        btLogin = findViewById(R.id.btnLogin);
        btLogarse = findViewById(R.id.btnLogarse);

        btLogarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), crearUsuari.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.btnEn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LANG_CURRENT.equals("es") || LANG_CURRENT.equals("ca")) {
                    changeLang(getBaseContext(), "en");
                    updateResources(getBaseContext(), "en");
                }
                recreate();
                startActivity(new Intent(MainActivity.this, MainActivity.class));

            }
        });

        findViewById(R.id.btnCat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LANG_CURRENT.equals("es") || LANG_CURRENT.equals("en")) {
                    changeLang(getBaseContext(), "ca");
                    updateResources(getBaseContext(), "ca");
                }
                recreate();
                startActivity(new Intent(MainActivity.this, MainActivity.class));

            }
        });

        findViewById(R.id.btnEsp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LANG_CURRENT.equals("en") || LANG_CURRENT.equals("ca")) {
                    changeLang(MainActivity.this, "es");
                    updateResources(getBaseContext(), "es");
                }
                recreate();
                startActivity(new Intent(MainActivity.this, MainActivity.class));
            }
        });
    }

    public void changeLang(Context context, String lang) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("Language", lang);
        editor.apply();
    }

    @Override
    protected void attachBaseContext(Context newBase) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(newBase);
        LANG_CURRENT = preferences.getString("Language", "en");

        super.attachBaseContext(MyContextWrapper.wrap(newBase, LANG_CURRENT));
    }

    private static boolean updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());

        return true;
    }


    public void metodoLogin(View view) {
        BDProjecte bdprojecte = new BDProjecte(getApplicationContext());
        SQLiteDatabase db = bdprojecte.getWritableDatabase();
        String usuario = eUsername.getText().toString();
        String password = ePassword.getText().toString();
        validacion = db.rawQuery("SELECT foto,usuario,password,sexe,correo,nom, cognom FROM USUARIOS WHERE USUARIO='" + usuario + "'AND PASSWORD='" + password + "'", null);
        if (validacion.moveToFirst()) {
            int foto = validacion.getInt(0);
            String usua = validacion.getString(1);
            String pass = validacion.getString(2);
            String sex = validacion.getString(3);
            String corr = validacion.getString(4);
            String nom = validacion.getString(5);
            String cog = validacion.getString(6);

            if (usuario.equals(usua) && password.equals(pass)) {
                Intent intent = new Intent(getApplicationContext(), decibelRecorder.class);
                intent.putExtra("variable_foto",foto);
                intent.putExtra("variable_usuario", usua);
                intent.putExtra("variable_password", pass);
                intent.putExtra("variable_correo", corr);
                intent.putExtra("variable_sexe", sex);
                intent.putExtra("variable_nom", nom);
                intent.putExtra("variable_cog", cog);
                startActivity(intent);

                eUsername.setText("");
                ePassword.setText("");
            }
            } else {
                Toast.makeText(getApplicationContext(), "Incorrect User", Toast.LENGTH_SHORT).show();
            }
    }

    public void metodoRestablecerContrasena(View view) {
        Intent intent = new Intent(this,restablecerContrasena.class);
        startActivity(intent);
    }
}
