package cat.copernic.decibelalert;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.text.DecimalFormat;


public class AudioFragment extends Fragment {
    Button btIniciar, btSalir, btView;
    String ipadress;
    private Runnable runnable;
    private MyClientTask myClientTask;
    int port;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            ipadress = getArguments().getString("ipAdresa", "nada");
            port = getArguments().getInt("ipPort");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_audio, container, false);
        btIniciar = rootView.findViewById(R.id.btnIniciar);
        btSalir = rootView.findViewById(R.id.btnSalir);
        btView = rootView.findViewById(R.id.btnView);

        btSalir.setEnabled(false);

        if (btSalir.isClickable()) {
            btView.setEnabled(true);
        }

        btIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyClientTask myClientTask = new MyClientTask(ipadress,port,1,runnable);
                new Thread(myClientTask).start();
                // myClientTask.execute();
                btSalir.setEnabled(true);
                btIniciar.setEnabled(false);
            }
        });

        btSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyClientTask myClientTask = new MyClientTask(ipadress,port,2,runnable);
                new Thread(myClientTask).start();
            }
        });


        btView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MostrarDataJson.class);
                startActivity(intent);
            }
        });

        runnable = new Runnable() {
            @Override
            public void run() {
                if(myClientTask.obtenerAcceso() == 1){
                    switch (myClientTask.obtenerOpcion()){
                        case 1:
                            System.out.println("Iniciar Thread");
                            break;
                        case 2:
                            System.out.println("Parar Thread");
                            break;
                    }
                }
            }
        };

        return rootView;
    }

    /*public void msgPendientes() throws IOException {
        int nMensajes = myClientTask.msgPendientes();
        Toast.makeText(getContext(), "Hay " + nMensajes + "pendientes en el servidor.", Toast.LENGTH_LONG).show();
    }*/


}
