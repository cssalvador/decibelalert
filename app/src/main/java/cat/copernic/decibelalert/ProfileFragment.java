package cat.copernic.decibelalert;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ProfileFragment extends Fragment {
    private TextView etUserPerfil,etNomsPerfil, etCorreuPerfil, etSexePerfil, etPasswordPerfil;
    private String usuario;
    private String nombre, apellido,correo,sexe,password;
    private Button cambiarPerfil;
    public ProfileFragment(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            usuario = getArguments().getString("user", "nada");
            nombre = getArguments().getString("nombre", "nada");
            apellido = getArguments().getString("apellido","nada");
            correo = getArguments().getString("correo","nada");
            sexe = getArguments().getString("sexe","nada");
            password = getArguments().getString("password","nada");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_perfil,container,false);
        etUserPerfil = rootview.findViewById(R.id.etUserPerfil);
        etNomsPerfil = rootview.findViewById(R.id.etNomsPerfil);
        etCorreuPerfil = rootview.findViewById(R.id.etCorreuPerfil);
        etSexePerfil = rootview.findViewById(R.id.etSexePerfil);
        etPasswordPerfil = rootview.findViewById(R.id.etPasswordPerfil);
        cambiarPerfil = rootview.findViewById(R.id.btnCanviarDades);

        cambiarPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(),cambiarPerfil.class);
                intent.putExtra("variable_cambiarNom",nombre);
                intent.putExtra("variable_cambiarApellido",apellido);
                intent.putExtra("variable_cambiarUsuario",usuario);
                intent.putExtra("variable_cambiarCorreo",correo);
                intent.putExtra("variable_cambiarSexe",sexe);
                intent.putExtra("variable_cambiarPassword",password);
                startActivity(intent);
            }
        });

        etUserPerfil.setText("@"+usuario);
        etUserPerfil.setEnabled(false);
        etNomsPerfil.setText(nombre + " "+apellido);
        etNomsPerfil.setEnabled(false);
        etCorreuPerfil.setText(correo);
        etCorreuPerfil.setEnabled(false);
        etSexePerfil.setText(sexe);
        etSexePerfil.setEnabled(false);
        etPasswordPerfil.setText(password);
        etPasswordPerfil.setEnabled(false);
        return rootview;
    }

}
